#| GNU Guix manifest

This file is a GNU Guix manifest file. It can be used with GNU Guix to
create a profile or an environment to work on the project. |#

(use-modules (gnu packages))


(define PACKAGES
  (list "inkscape"
        "adwaita-icon-theme"
        "fontconfig"
        "font-fira-sans"
        ;; NOTE: Oswald is pending inclusion in Guix, see patch
        ;; https://issues.guix.gnu.org/68640
        "font-oswald"
        ;; Only necessary for its CMYK color profile
        "krita"))

(specifications->manifest PACKAGES)
