#+TITLE: Themes

This directory contains themes for Guix websites and other themable
artifacts. Themes are folders containing style sheets, fonts, images,
and other assets that define the visual, aural and haptic identity of
the artifacts.
