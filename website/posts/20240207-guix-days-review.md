title: Guix Days 2024 and FOSDEM recap
slug: guix-days-2024-recap
date: 2024-02-10 18:00:00
author: Steve George
tags: FOSDEM, Guix Days, Talks, Community
---

Guix contributors and users got together in Brussels to explore Guix's status, chat about new ideas and spend some time together enjoying Belgian beer! Here's a recap of what was discussed.


# Day 1

The first day kicked off with an update on the project's health, given by Efraim Flashner representing the project's Maintainer collective. Efraim relayed that the project is doing well, with lots of exciting new features coming into the archive and new users taking part. It was really cool listening to all the new capabilities - thank-you to all our volunteer contributors who are making Guix better! Efraim noted that the introduction of [Teams](https://guix.gnu.org/manual/en/html_node/Teams.html) has improved collaboration - equally, that there's plenty of areas we can improve. For example, concern remains over the "bus factor" in key areas like infrastructure. There's also a desire to release more often as this provides an updated installer and lets us talk about new capabilities.

Christopher Baines gave a general talk about the QA infrastructure and the ongoing work to develop automated builds. Chris showed a diagram of the way the [services interact](https://qa.guix.gnu.org/README#org5dde7a9) which shows how complex it is. Increasing automation is very valuable for users and contributors, as it removes tedious and unpleasant drudgery!

Then, Julien Lepiller, representing the [Guix Foundation](https://foundation.guix.info/), told us about the work it does. Julien also brought some great stickers! The Guix Foundation is a non-profit association that can receive donations, host activities and support the Guix project. Did you know that it's simple and easy to join? Anyone can do so by simply **[filling in the form and paying the 10 Euro membership fee](https://foundation.guix.info/statutes/membershipform.txt)**. Contact the Guix Foundation if you'd like to know more.

The rest of the day was taken up with small groups discussing topics:

 - **Goblins, Hoot and Guix**: Christine Lemmer-Webber gave an introduction to
   the [Spritely Institute's](https://spritely.institute/) mission to create
   decentralized networks and community infrastructure that respects user freedom
   and security. There was a lot of interesting discussion about how the
   network capabilities could be used in Guix, for example enabling distributed
   build infrastructure.

 - **Infrastructure**: There was a working session on how the projects
   infrastructure works and can be improved. Christopher Baines has been
   putting lots of effort into the QA and build infrastructure.

 - **Guix Home**: Gábor Boskovits coordinated a session on Guix Home. It was
   exciting to think about how Guix Home introduces the "Guix way" in a 
   completely different way from packages. This could introduce a whole new
   audience to the project. There was interest in improving the overall
   experience so it can be used with other distributions
   (e.g. Fedora, Arch Linux, Debian and Ubuntu).

 - **Release management**: Julien Lepiller led us through a discussion of
   release management, explaining the ways that all the parts fit together. The
   most important part that has to be done is testing the installation image 
   which is a manual process.


# Day 2

The second day's sessions:

- **Funding**: A big group discussed funding for the project. Funding is
  important because it determines many aspects of what the group can achieve.
  Guix is a global project so there are pools of money in the United States and
  Europe (France). Andreas Enge and Julien Lepiller represented the group that
  handle finance, giving answers on the practical elements. Listening to their
  description of this difficult and involved work, I was struck how grateful
  we all are that they're willing to do it!

- **Governance**: Guix is a living project that continues to grow and evolve.
  The governance discussion concerned how the project continues to chart a
  clear direction, make good decisions and bring both current and new users on
  the journey. There was reflection on the need for accountability and quick
  decision making, without onerous bureaurcacy, while also acknowledging that
  everyone is a volunteer. There was a lot of interest in how groups can join
  together, perhaps using approaches like [Sociocracy](https://en.wikipedia.org/wiki/Sociocracy).

  Simon Tournier has been working on an [RFC process](https://issues.guix.gnu.org/issue/66844),
  which the project will use to discuss major changes and make decisions.
  Further discussion is taking place on the development mailing-list if you'd
  like to take part.

- **Alternative Architectures**: The Guix team continues to work on
  alternative architectures. Efraim had his 32-bit PowerPC (Powerbook G4) with
  him, and there's continued work on PowerPC64, ARM64 and RISC-V 64. The big
  goal is a complete source bootscrap across all architectures.

- **Hurd**: Janneke Nieuwenhuizen led a discussion around
  [GNU Hurd](https://www.gnu.org/software/hurd/), which is a microkernel-based
  architecture. Activity has increased in the last couple of years, and there's
  support for SMP and 64-bit (x86) is work in progress. There's lots of ideas
  and excitement about getting Guix to work on Hurd.

- **Guix CLI improvements**: Jonathan coordinated a discussion about the state of the Guix CLI. A consistent, self-explaining and intuitive experience is important for our users. There are 39 top-level commands, that cover all the functionality from package management through to environment and system creation! Various improvements were discussed, such as making extensions available and improving documentation about the REPL work-flow.


# FOSDEM 2024 videos

Guix Days 2024 took place just before FOSDEM 2024. FOSDEM was a fantastic two days of interesting talks and conversations. If you'd like to watch the GUIX-related talks the videos are being put online:

- [**Making reproducible and publishable large-scale HPC experiments**](https://fosdem.org/2024/schedule/event/fosdem-2024-2651-making-reproducible-and-publishable-large-scale-hpc-experiments/)
    by Philippe Swartvagher.

- [**Scheme in the Browser with Guile Hoot and WebAssembly**](https://fosdem.org/2024/schedule/event/fosdem-2024-2339-scheme-in-the-browser-with-guile-hoot-and-webassembly/)
    by Robin Templeton.

- [**RISC-V Bootstrapping in Guix and Live-Bootstrap**](https://fosdem.org/2024/schedule/event/fosdem-2024-1755-risc-v-bootstrapping-in-guix-and-live-bootstrap/)
    by Ekaitz Zarraga.

- [**Self-hosting and autonomy using guix-forge**](https://fosdem.org/2024/schedule/event/fosdem-2024-2560-self-hosting-and-autonomy-using-guix-forge/)
    by Arun Isaac.

- [**Spritely, Guile, Guix: a unified vision for user security**](https://fosdem.org/2024/schedule/event/fosdem-2024-2331-spritely-guile-guix-a-unified-vision-for-user-security/)
    by Christine Lemmer-Webber.

- [**Supporting architecture psABIs with GNU Guix**](https://fosdem.org/2024/schedule/event/fosdem-2024-2927-supporting-architecture-psabis-with-gnu-guix/)
    by Efraim Flashner.

# Join Us

There's lots happening in Guix and many ways to get involved. We're a small and friendly project that values user freedom and a welcoming community. If this recap has inspired your interest, take a look at the [raw notes](https://git.savannah.gnu.org/cgit/guix/maintenance.git/tree/doc/guix-days-2024) and [**join us!**](https://guix.gnu.org/en/contribute/)


