title: The European Union must keep funding free software
author: The Guix Project
date: 2024-07-23 17:30:00
tags: Community
---
Guix is the fruit of a combination of volunteer work by an amazing
number of people, work paid for by employers, but also work sponsored by
public institutions.  The European Commission’s [Next Generation
Internet](https://www.ngi.eu) (NGI) calls have been instrumental in that
regard.  News that NGI funding could vanish came to us as a warning
signal.

Since 2020, NGI has supported many free software projects, allowing for
significant strides on important topics that would otherwise be hard to
fund.  As an example, here are some of the NGI grants that directly
benefited Guix and related projects:

 - the [full-source
   bootstrap](https://nlnet.nl/project/GNUMes-fullsource), which
   includes groundwork not just in Guix but crucially in
   [Mes](https://www.gnu.org/software/mes) and sister projects ([blog
   post](https://guix.gnu.org/en/blog/2023/the-full-source-bootstrap-building-from-source-all-the-way-down/));
 - [porting Guix to the RISC-V
   architecture](https://nlnet.nl/project/Guix-Riscv64/);
 - [porting GNU Mes and associated projects to
   RISC-V](https://nlnet.nl/project/GNUMes-RISCV/) and
   [AArch64](https://nlnet.nl/project/GNUMes-ARM_RISC-V);
 - [porting the full-source bootstrap to the RISC-V
   architecture](https://nlnet.nl/project/GNUMes-RISCV-bootstrap/);
 - the [Cuirass continuous integration
   tool](https://nlnet.nl/project/Cuirass) ([blog
   post](https://guix.gnu.org/en/blog/2021/cuirass-10-released/));
 - the [Guile implementation of the Guix build
   daemon](https://nlnet.nl/project/GuixDaemon-Guile/) ([blog
   post](https://guix.gnu.org/en/blog/2023/a-build-daemon-in-guile/));
 - [distributed system daemon management with the Shepherd
   and Goblins](https://nlnet.nl/project/DistributedShepherd/), under
   the aegis of the Spritely Institute ([blog
   post](https://spritely.institute/news/spritely-nlnet-grants-december-2023.html)).
 - a [new garbage collector for
   Guile](https://nlnet.nl/project/Whippet/), the Scheme implementation
   that Guix builds upon.

Over the years, NGI has more than demonstrated that public financial
support for free software development makes a difference.  We strongly
believe that this support must continue, that it must strengthen the
development of innovative software where user autonomy and freedom is a
central aspect.

For these reasons, the Guix project joins a growing number of projects
and organizations in signing the following open letter to the European
Commission.

> The open letter below was initially published by [petites
> singularités](https://ps.zoethical.org/pub/lettre-publique-aux-ncp-au-sujet-de-ngi/).
> English translation provided by
> [OW2](https://www.ow2.org/view/Events/The_European_Union_must_keep_funding_free_software_open_letter).

## Open Letter to the European Commission

Since 2020, Next Generation Internet ([NGI](https://www.ngi.eu)) programmes, part of European Commission's Horizon programme, fund free software in Europe using a cascade funding mechanism (see for example NLnet's [calls](https://www.nlnet.nl/commonsfund)). This year, according to the Horizon Europe working draft detailing funding programmes for 2025, we notice that Next Generation Internet is not mentioned any more as part of Cluster 4.

NGI programmes have shown their strength and importance to supporting the European software infrastructure, as a generic funding instrument to fund digital commons and ensure their long-term sustainability. We find this transformation incomprehensible, moreover when NGI has proven efficient and economical to support free software as a whole, from the smallest to the most established initiatives. This ecosystem diversity backs the strength of European technological innovation, and maintaining the NGI initiative to provide structural support to software projects at the heart of worldwide innovation is key to enforce the sovereignty of a European infrastructure.
Contrary to common perception, technical innovations often originate from European rather than North American programming communities, and are mostly initiated by small-scaled organisations.

Previous Cluster 4 allocated 27 million euros to:

- "Human centric Internet aligned with values and principles commonly shared in Europe" ;
- "A flourishing internet, based on common building blocks created within NGI, that enables better control of our digital life" ;
- "A structured ecosystem of talented contributors driving the creation of new internet commons and the evolution of existing internet commons".

In the name of these challenges, more than 500 projects received NGI funding in the first 5 years, backed by 18 organisations managing these European funding consortia.

NGI contributes to a vast ecosystem, as most of its budget is allocated to fund third parties by the means of open calls, to structure commons that cover the whole Internet scope - from hardware to application, operating systems, digital identities or data traffic supervision. This third-party funding is not renewed in the current program, leaving many projects short on resources for research and innovation in Europe.

Moreover, NGI allows exchanges and collaborations across all the Euro zone countries as well as "widening countries"¹, currently both a success and an ongoing progress, likewise the Erasmus programme before us. NGI also contributes to opening and supporting longer relationships than strict project funding does. It encourages implementing projects funded as pilots, backing collaboration, identification and reuse of common elements across projects, interoperability in identification systems and beyond, and setting up development models that mix diverse scales and types of European funding schemes.

While the USA, China or Russia deploy huge public and private resources to develop software and infrastructure that massively capture private consumer data, the EU can't afford this renunciation.
Free and open source software, as supported by NGI since 2020, is by design the opposite of potential vectors for foreign interference. It lets us keep our data local and favors a community-wide economy and know-how, while allowing an international collaboration.

This is all the more essential in the current geopolitical context: the challenge of technological sovereignty is central, and free software allows to address it while acting for peace and sovereignty in the digital world as a whole.

In this perspective, we urge you to claim for preserving the NGI programme as part of the 2025 funding programme.

¹ As defined by Horizon Europe, widening Member States are Bulgaria, Croatia, Cyprus, Czechia, Estonia, Greece, Hungary, Latvia, Lituania, Malta, Poland, Portugal, Romania, Slovakia, and Slovenia. Widening associated countries (under condition of an association agreement) include Albania, Armenia, Bosnia, Feroe Islands, Georgia, Kosovo, Moldavia, Montenegro, Morocco, North Macedonia, Serbia, Tunisia, Turkeye, and Ukraine. Widening overseas regions are Guadeloupe, French Guyana, Martinique, Reunion Island, Mayotte, Saint-Martin, The Azores, Madeira, the Canary Islands. 
