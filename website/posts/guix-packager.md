title: Write package definitions in a breeze
author: Ludovic Courtès, Philippe Virouleau
tags: Scheme API, Programming interfaces
date: 2023-11-24 14:30
---

More than 28,000 packages are available in Guix today, not counting
third-party channels.  That’s a lot—the [5th largest GNU/Linux
distro](https://repology.org/)!  But it’s nothing if the one package you
care about is missing.  So even you, dear reader, may one day find
yourself defining a package for your beloved deployment tool.  This post
introduces a new tool poised to significantly lower the barrier to
writing new packages.

# Introducing Guix Packager

[Defining
packages](https://guix.gnu.org/manual/devel/en/html_node/Defining-Packages.html)
for Guix is not all that hard but, as always, it’s much harder the first
time you do it, especially when starting from a blank page and/or not
being familiar with the programming environment of Guix.  [Guix
Packager](https://guix-hpc.gitlabpages.inria.fr/guix-packager/) is a new
web user interface to get you started—[try
it!](https://guix-hpc.gitlabpages.inria.fr/guix-packager/).  It arrived
right in time as an aid to the [packaging
tutorial](https://hpc.guix.info/events/2023/workshop/program/#how-to-get-started-writing-guix-packages)
given last week at the Workshop on Reproducible Software Environments.

![Screenshot showing the Guix Packager interface.](/static/blog/img/guix-packager.gif)

The interface aims to be intuitive: fill in forms on the left
and it produces a correct, ready-to-use package definition on the right.
Importantly, it helps you avoid pitfalls that trip up many newcomers:

  - When you add a dependency in one of the “Inputs” fields, it adds the
    right variable name in the generated code *and* imports the right
    [package
    module](https://guix.gnu.org/manual/devel/en/html_node/Package-Modules.html).
  - Likewise, you can choose a license and be sure the `license` field
    will refer to the right variable representing that license.
  - You can turn tests on and off, and add configure flags.  These
    translate to a valid `arguments` field of your package, letting you
    discover the likes of [keyword
    arguments](https://guix.gnu.org/cookbook/en/html_node/A-Scheme-Crash-Course.html)
    and
    [G-expressions](https://guix.gnu.org/manual/en/html_node/G_002dExpressions.html)
    without having to first dive into the manual.

Pretty cool, no?

# Implementation

All the credit for this tool goes to co-worker and intrepid hacker
Philippe Virouleau.  A unique combination of paren aversion and web
development superpowers—unique in the Guix community—led
Philippe to develop the whole thing in a glimpse (says Ludovic!).

The purpose was to provide a single view to be able to edit a package recipe,
therefore the application is a *single-page application* (SPA) written in
using the UI library Philippe is most comfortable with: [React](https://react.dev/),
and [MaterialUI](https://mui.com/material-ui/) for styling the components.
It's built with [TypeScript](https://www.typescriptlang.org/), and the library
part actually defines all the types needed to manipulate Guix packages and their
components (such as build systems or package sources).
One of the more challenging parts was to be able to provide fast and helpful “search as you
type” results over the 28k+ packages. It required a combination of
MaterialUI's [virtualized inputs](https://mui.com/material-ui/react-autocomplete/#virtualization),
as well as caching the packages data in the browser's local storage,
when possible (packaging metadata itself is fetched from
`https://guix.gnu.org/packages.json`, a generic representation of the
current package set).

While the feature set provides a great starting point, there are still a few
things that may be worth implementing.  For instance, only the GNU and
CMake build systems are supported so far; it would make sense to include
a few others (Python-related ones might be good candidates).

Running a local (development) version of the application can happen on
top of Guix, since—obviously—it's been developed with the `node`
version packaged in Guix, using the quite standard `packages.json` for
JavaScript dependencies installed through `npm`.  [Contributions
welcome!](https://gitlab.inria.fr/guix-hpc/guix-packager/)

# Lowering the barrier to entry

This neat tool complements a set of steps we’ve taken over time to make
packaging in Guix approachable.  Indeed, while package definitions are
actually code written in the Scheme language, the `package` “language”
was designed [from the get-go](https://arxiv.org/abs/1305.4584) to be
fully declarative—think JSON with parens instead of curly braces and
semicolons.  More recently we [simplified the way package inputs are
specified](https://guix.gnu.org/en/blog/2021/the-big-change/) with an
eye on making package definitions less intimidating.

The [`guix import`
command](https://guix.gnu.org/manual/devel/en/html_node/Invoking-guix-import.html)
also exists to make it easier to simplify packaging: it can generate a
package definition for anything available in other package
repositories such as PyPI, CRAN, Crates.io, and so forth.  If your
preference goes to curly braces rather than parens, it can also [convert
a JSON package
description](https://guix.gnu.org/manual/devel/en/html_node/Invoking-guix-import.html#index-JSON_002c-import)
to Scheme code.  Once you have your first `.scm` file, `guix build`
prints hints for common errors such missing module imports (those
`#:use-module` stanzas).  We also put effort into providing [reference
documentation](https://guix.gnu.org/manual/devel/en/html_node/Defining-Packages.html),
a [video
tutorial](https://guix.gnu.org/en/videos/2020/packaging-part-two/), and
a [tutorial for more complex
packages](https://guix.gnu.org/cookbook/en/html_node/Packaging-Tutorial.html).

Do share your experience [with us](https://guix.gnu.org/contact) and
until then, happy packaging!

# Acknowledgments

Thanks to Felix Lechner and Timothy Sample for providing feedback on an
earlier draft of this post.

#### About GNU Guix

[GNU Guix](https://guix.gnu.org) is a transactional package manager and
an advanced distribution of the GNU system that [respects user
freedom](https://www.gnu.org/distros/free-system-distribution-guidelines.html).
Guix can be used on top of any system running the Hurd or the Linux
kernel, or it can be used as a standalone operating system distribution
for i686, x86_64, ARMv7, AArch64 and POWER9 machines.

In addition to standard package management features, Guix supports
transactional upgrades and roll-backs, unprivileged package management,
per-user profiles, and garbage collection.  When used as a standalone
GNU/Linux distribution, Guix offers a declarative, stateless approach to
operating system configuration management.  Guix is highly customizable
and hackable through [Guile](https://www.gnu.org/software/guile)
programming interfaces and extensions to the
[Scheme](http://schemers.org) language.
