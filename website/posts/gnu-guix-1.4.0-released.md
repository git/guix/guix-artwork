title: GNU Guix 1.4.0 released
date: 2022-12-19 15:30:00
author: Ludovic Courtès
slug: gnu-guix-1.4.0-released
tags: Releases
---

We are pleased to announce the release of GNU Guix version 1.4.0!

The release comes with ISO-9660 installation images, a virtual machine
image, and with tarballs to install the package manager on top of your
GNU/Linux distro, either from source or from binaries—check out the
[download page](https://guix.gnu.org/en/download).  Guix users can
update by running `guix pull`.

It’s been 18 months since the [previous
release](https://guix.gnu.org/en/blog/2021/gnu-guix-1.3.0-released/).
That’s a lot of time, reflecting both the fact that, as a *rolling
release*, users continuously get new features and update by running
`guix pull`; but let’s face it, it also shows an area where we could and
should collectively improve our processes.  During that time, Guix
received about 29,000 commits by 453 people, which includes important
new features as we’ll see; the project also [changed
maintainers](https://guix.gnu.org/en/blog/2022/gnu-guix-maintainer-rotation/),
structured cooperation as
[teams](https://guix.gnu.org/manual/en/html_node/Teams.html), and
[celebrated its ten-year
anniversary](https://guix.gnu.org/en/blog/2022/wrapping-up-ten-years-of-guix-in-paris/)!

![A happy frog sitting at a Guix-powered computer with an almighty benevolent Guix humanoid in its back.](/static/blog/img/gnu-guix-a-frogs-dream.jpg)

> _Illustration by Luis Felipe, published under CC-BY-SA 4.0._

This post provides highlights for all the hard work that went into this
release—and yes, these are probably the longest release notes in Guix’s
history, so make yourself comfortable, relax, and enjoy.

> Bonus! Here’s a
> [chiptune](https://opengameart.org/sites/default/files/Departure.ogg)
> ([by Trevor Lentz](https://opengameart.org/content/departure), under
> CC-BY-SA 3.0) our illustrator Luis Felipe recommends that you listen
> to before going further.

# Improved software environment management

One area where Guix shines is the management of **software
environments**.  The `guix environment` command was designed for that
but it suffered from a clumsy interface.  It is [now superseded by `guix
shell`](https://guix.gnu.org/en/blog/2021/from-guix-environment-to-guix-shell/),
though we are committed to keeping `guix environment` until at least May
1st, 2023.  `guix shell` is a tool that’s interesting to developers, but
it’s also a useful tool when you’re willing to try out software without
committing it to your profile with `guix install`.  Let’s say you want
to play [SuperTuxKart](https://supertuxkart.net) but would rather not
have it in your profile because, hey, it’s a professional laptop; here’s
how you would launch it, fetching it first if necessary:

```
guix shell supertuxkart -- supertuxkart
```

In addition to providing a simpler interface, `guix shell` significantly
improves performance through caching.  It also simplifies developer
workflows by automatically recognizing `guix.scm` and `manifest.scm`
files present in a directory: drop one of these in your project and
other developers can get started hacking just by running `guix shell`,
without arguments.  Speaking of which: `--export-manifest` will get you
started by “converting” command-line arguments into a manifest.  [Read
more about `guix shell` in the
manual](https://guix.gnu.org/manual/en/html_node/Invoking-guix-shell.html).

Another `guix shell` innovation is optional **emulation of the
filesystem hierarchy standard (FHS)**.  The
[FHS](https://refspecs.linuxfoundation.org/fhs.shtml) specifies
locations for different file categories—`/bin` for essential command
binaries, `/lib` for libraries, and so on.  Guix with its
[store](https://guix.gnu.org/manual/en/html_node/The-Store.html)
does not adhere to the FHS, which prevents users from running programs
that assume FHS adherence.  The new `--emulate-fhs` (or `-F`) flag of
`guix shell`, combined with `--container` (`-C`), instructs it to create
a container environment that follows the FHS.  This is best illustrated
with this example, where the `ls` command of the `coreutils` package
appears right under `/bin`, as if we were on an FHS system like Debian:

```
$ guix shell -CF coreutils -- /bin/ls -1p /
bin/
dev/
etc/
gnu/
home/
lib/
lib64
proc/
sbin/
sys/
tmp/
usr/
```

Another big new feature is **Guix Home**.  In a nutshell, Home brings
the declarative nature of Guix System to your *home environment*: it
lets you declare all the aspects of your home environments—“dot files”,
services, and packages—and can [instantiate that environment, in your
actual `$HOME` or in a
container](https://guix.gnu.org/en/blog/2022/keeping-ones-home-tidy/).

If you’re already maintaining your dot files under version control, or
if you would like to keep things under control so you don’t have to
spend days or weeks configuring again next time you switch laptops, this
is the tool you need.  Check out [this excellent
introduction](https://10years.guix.gnu.org/video/an-introduction-to-guix-home/)
that David Wilson gave at the Ten Years celebration, and [read more
about Guix Home in the
manual](https://guix.gnu.org/manual/en/html_node/Declaring-the-Home-Environment.html).

[**Package transformation
options**](https://guix.gnu.org/manual/en/html_node/Package-Transformation-Options.html)
give users fine control over the way packages are built.  The new
`--tune` option enables tuning of packages **for a specific CPU
micro-architecture**; this [enables the use of the newest
single-instruction multiple-data (SIMD)
instructions](https://hpc.guix.info/blog/2022/01/tuning-packages-for-a-cpu-micro-architecture/),
such as AVX-512 on recent AMD/Intel CPUs, which can make a significant
difference for some workloads such as linear algebra computations.

Since the 1.3.0 release, the project started maintaining [**an
alternative build
farm**](https://guix.gnu.org/en/blog/2021/substitutes-now-also-available-from-bordeauxguixgnuorg/)
at `https://bordeaux.guix.gnu.org`.  It’s independent from the build
farm at `ci.guix.gnu.org` (donated and hosted by the Max Delbrück Center
for Molecular Medicine in Berlin, Germany), which has two benefits: it
lets us [challenge substitutes produced by each
system](https://guix.gnu.org/manual/en/html_node/Invoking-guix-challenge.html),
and it provides redundancy should one of these two build farms go down.
Guix is now configured by default to fetch substitutes from any of these
two build farms.  In addition, a
[bug](https://issues.guix.gnu.org/57978) was fixed, ensuring that Guix
gracefully switches to another substitute provider when one goes down.

Those who’ve come to enjoy declarative deployment of entire fleets of
machines will probably like the new [**`--execute` option of `guix
deploy`**](https://guix.gnu.org/manual/en/html_node/Invoking-guix-deploy.html).

# Stronger distribution

The distribution itself has seen lots of changes.  First, the **Guix
System installer** received a number of bug fixes and it now includes a
new mechanism that allows users to automatically report useful debugging
information in case of a crash.  This will help developers address bugs
that occur with unusual configurations.

**Application startup has been reduced** thanks to [a new
per-application dynamic linker
cache](https://guix.gnu.org/en/blog/2021/taming-the-stat-storm-with-a-loader-cache/)
that drastically reduces the number of `stat` and `open` calls due to
shared library lookup (we’re glad it
[inspired](https://fzakaria.com/2022/09/12/making-runpath-redundant-for-nix.html)
[others](https://stoppels.ch/2022/08/04/stop-searching-for-shared-libraries.html)).

Guix System is now using [**version 0.9 of the
GNU Shepherd**](https://www.gnu.org/software/shepherd), which addresses
shortcomings, improves logging, and adds features such as systemd-style
service activation and inetd-style service startup.  Speaking of
services, the [new `guix system edit`
sub-command](https://guix.gnu.org/manual/en/html_node/Invoking-guix-system.html)
provides an additional way for users to inspect services, completing
`guix system search` and `guix system extension-graph`.

There are 15 [**new system
services**](https://guix.gnu.org/manual/en/html_node/Services.html) to
choose from, including
[Jami](https://guix.gnu.org/manual/en/html_node/Telephony-Services.html),
[Samba](https://guix.gnu.org/manual/en/html_node/Samba-Services.html),
[fail2ban](https://guix.gnu.org/manual/en/html_node/Miscellaneous-Services.html#Fail2Ban-service),
and
[Gitile](https://guix.gnu.org/manual/en/html_node/Version-Control-Services.html#Gitile-Service),
to name a few.

A new interface is available to [**declare swap
space**](https://guix.gnu.org/manual/en/html_node/Swap-Space.html)
in operating system configurations.  This interface is more expressive
and more flexible than what was available before.

Similarly, the interface to declare [**static networking
configuration**](https://guix.gnu.org/manual/en/html_node/Networking-Setup.html)
has been overhauled.  On GNU/Linux, it lets you do roughly the same as
the `ip` command, only in a declarative fashion and with static checks
to prevent you from deploying obviously broken configurations.

![Screenshot of GNOME 42.](/static/blog/img/gnome-42.png)

More than **5,300 packages were added** for a total of almost 22,000
packages, making Guix one of the top-ten biggest distros [according to
Repology](https://repology.org/).  Among the many noteworthy package
upgrades and addition, **GNOME 42** is now available.  KDE is not there
yet but tens of KDE packages have been added so we’re getting closer;
Qt 6 is also available.  The distribution also comes with GCC 12.2.0,
GNU libc 2.33, Xfce 4.16, Linux-libre 6.0.10, LibreOffice 7.4.3.2, and
Emacs 28.2 (with just-in-time compilation support!).

In other news, motivated by the fact that Python 2 officially reached
“end of life” in 2020, more than 500 **Python 2 packages** were
removed—those whose name starts with `python2-`.  This includes “big
ones” like `python2-numpy` and `python2-scipy`.  Those who still need
these have two options: using [`guix
time-machine`](https://guix.gnu.org/manual/en/html_node/Invoking-guix-time_002dmachine.html)
to jump to an older commit that contains the packages they need, or
using the [Guix-Past](https://gitlab.inria.fr/guix-hpc/guix-past)
channel to build some of those old packages in today’s
environments—scientific computing is one area where this may come in
handy.

On top of that, the Web site features a [**new package
browser**](https://packages.guix.gnu.org)—at last!  Among other things,
the package browse provides stable package URLs like
`https://packages.guix.gnu.org/packages/PACKAGE`.

The [`NEWS`
file](https://git.savannah.gnu.org/cgit/guix.git/tree/NEWS?h=version-1.4.0)
lists additional noteworthy changes and bug fixes you may be
interested in.

# More documentation

As with past releases, we have worked on documentation to make Guix more
approachable.  “How-to” kind of sections have been written or improved,
such as:

  - [“Writing
    Manifests”](https://guix.gnu.org/manual/en/html_node/Writing-Manifests.html)
  - [“Replicating
    Guix”](https://guix.gnu.org/manual/en/html_node/Replicating-Guix.html)
  - [“Using TeX and
    LaTeX”](https://guix.gnu.org/manual/en/html_node/Using-TeX-and-LaTeX.html)
  - [“System Troubleshooting
    Tips”](https://guix.gnu.org/manual/en/html_node/System-Troubleshooting-Tips.html)
  - [“Foreign
    Architectures”](https://guix.gnu.org/manual/en/html_node/Foreign-Architectures.html)
  - [“Using Guix
    Interactively”](https://guix.gnu.org/manual/en/html_node/Using-Guix-Interactively.html)

The [Cookbook](https://guix.gnu.org/cookbook/en/html_node/) likewise
keeps receiving how-to entries, check it out!

The Guix reference manual is **fully translated** into
[French](https://guix.gnu.org/manual/fr/html_node/) and
[German](https://guix.gnu.org/manual/de/html_node/); 70% is available in
[Spanish](https://guix.gnu.org/manual/es/html_node/), and there are
[preliminary
translations](https://translate.fedoraproject.org/projects/guix/documentation-manual/)
in [Russian](https://guix.gnu.org/manual/ru/html_node/),
[Chinese](https://guix.gnu.org/manual/zh-cn/html_node/), and other
languages.  Guix itself is fully translated in French, with almost
complete translations in Brazilian Portuguese, German, Slovak, and
Spanish, and partial translations in [almost twenty other
languages](https://translate.fedoraproject.org/projects/guix/guix/).
Check out the manual on [how to
help](https://guix.gnu.org/manual/en/html_node/Translating-Guix.html)
or this [guided tour by translator in chief Julien
Lepiller](https://10years.guix.gnu.org/video/let-s-translate-guix-together-/)!


# Supporting long-term reproducibility

A salient feature of Guix is its support for reproducible software
deployment.  There are several aspects to that, one of which is being
able to retrieve source code [from the Software Heritage
archive](https://guix.gnu.org/en/blog/2019/connecting-reproducible-deployment-to-a-long-term-source-code-archive/).
While Guix was already able to fetch the source code of packages from
Software Heritage as a fallback, with version 1.4.0 **the source code of
Guix channels is automatically fetched from Software Heritage** if its
original URL has become unreachable.

In addition, Guix is now **able to retrieve and restore source code
tarballs** such as `tar.gz` files.  Software Heritage archives the
_contents_ of tarballs, but not tarball themselves.  This created an
impedance mismatch for Guix, where the majority of [package
definitions](https://guix.gnu.org/manual/en/html_node/Defining-Packages.html)
refer to tarballs and expect to be able to verify the content hash _of
the tarball itself_.  To bridge this gap, Timothy Sample developed
[Disarchive](https://ngyro.com/software/disarchive.html), a tool that
can (1) _extract_ tarball metadata, and (2) _assemble_
previously-extracted metadata and actual files to reconstruct a tarball,
as shown in the diagram below.

![Diagram showing Disarchive and Software Heritage.](/static/blog/img/disarchive-swh-diagram.png)

The Guix project has set up a [continuous
integration job](https://ci.guix.gnu.org/jobset/disarchive) to build a
_Disarchive database_, which is available at
[disarchive.gnu.org](https://disarchive.guix.gnu.org).  The database
includes metadata for all the tarballs packages refer to.  When a source
code tarball disappears, Guix now transparently retrieves tarball
metadata from Disarchive database, fetches file contents from Software
Heritage, and reconstructs the original tarball.  As of the
“[Preservation of Guix Report](https://ngyro.com/pog-reports/latest/)”
published in January 2022, almost 75% of the `.tar.gz` files packages
refer to are now fully archived with Disarchive and Software Heritage.
Running `guix lint -c archival PKG` will tell you about the archival
status of `PKG`.
You can read more in the [annual report of
Guix-HPC](https://hpc.guix.info/blog/2022/02/guix-hpc-activity-report-2021/).

This is a significant step forward to provide, for the first time, a
tool that can redeploy past software environments while maintaining the
connection between source code and binaries.

# Application bundles and system images

The [`guix pack`
command](https://guix.gnu.org/manual/en/html_node/Invoking-guix-pack.html)
to create “**application bundles**”—standalone application images—has
been extended: `guix pack -f deb` creates a standalone `.deb` package
that can be installed on Debian and derivative distros; the new
`--symlink` flag makes it create symlinks within the image.

At the system level, the [**new `guix system image`
command**](https://guix.gnu.org/manual/en/html_node/Invoking-guix-system.html#index-image_002c-creating-disk-images)
supersedes previously existing `guix system` sub-commands, providing a
single entry point to build images of all types: raw disk images, QCOW2
virtual machine images, ISO9660 CD/DVD images, Docker images, and even
[images for Microsoft’s Windows Subsystem for Linux
(WSL2)](https://othacehe.org/wsl-images-for-guix-system.html).  This
comes with a [high-level
interface](https://guix.gnu.org/manual/en/html_node/System-Images.html)
that lets you declare the type of image you want: the storage format,
partitions, and of course the operating system for that image.  To
facilitate its use, predefined image types are provided:

```
$ guix system image --list-image-types
The available image types are:

   - rock64-raw
   - pinebook-pro-raw
   - pine64-raw
   - novena-raw
   - hurd-qcow2
   - hurd-raw
   - raw-with-offset
   - iso9660
   - efi32-raw
   - wsl2
   - uncompressed-iso9660
   - efi-raw
   - docker
   - qcow2
   - tarball
```

That includes [for
example](https://guix.gnu.org/manual/en/html_node/Image-Modules.html)
an image type for the Pine64 machines and for the GNU/Hurd operating
system.  For example, this is how you’d create an QCOW2 virtual machine
image suitable for QEMU:

```
guix system image -t qcow2 my-operating-system.scm
```

… where `my-operating-system.scm` contains an [operating system
declaration](https://guix.gnu.org/manual/en/html_node/Using-the-Configuration-System.html).

Likewise, here’s how you’d create, on your x86_64 machine, an image for
your Pine64 board, ready to be transferred to an SD card or similar
storage device that the board will boot from:

```
guix system image -t pine64-raw my-operating-system.scm
```

The `pine64-raw` image type specifies that software in the image is
actually *cross-compiled* to `aarch64-linux-gnu`—that is, GNU/Linux on
an AArch64 CPU, with the appropriate [U-Boot
variant](https://packages.guix.gnu.org/packages/u-boot-pine64-lts) as
its bootloader.  Sky’s the limit!

# Nicer packaging experience

A significant change that packagers will immediately notice is
[**package
simplification**](https://guix.gnu.org/en/blog/2021/the-big-change/),
introduced shortly after 1.3.0.  The most visible effect is that package
definitions now look clearer:

```scheme
(package
  ;; …
  (inputs (list pkg-config guile-3.0)))  ;👍
```

… instead of the old baroque style with “input labels”:

```scheme
(package
  ;; …
  (inputs `(("pkg-config" ,pkg-config)   ;👎
            ("guile" ,guile-3.0))))
```

The [**new `guix style`
command**](https://guix.gnu.org/manual/en/html_node/Invoking-guix-style.html)
can automatically convert from the “old” style to the “new” style of
package inputs.  It can also reformat whole Scheme files following the
stylistic canons _du jour_, which is particularly handy when getting
started with the language.

That’s just the tip of the iceberg: the new [`modify-inputs`
macro](https://guix.gnu.org/manual/en/html_node/Defining-Package-Variants.html#index-modify_002dinputs)
makes package input manipulation easier and clearer, and one can use
[G-expressions](https://guix.gnu.org/manual/en/html_node/G_002dExpressions.html#G_002dExpressions)
for instance in package phases.  Read our [earlier
announcement](https://guix.gnu.org/en/blog/2021/the-big-change/) for
more info.  On top of that, the new _field sanitizer_ mechanism is used
to validate some fields; for instance, the `license` field is now
type-checked and the Texinfo syntax of `description` and `synopsis` is
validated, all without any run-time overhead in common cases.  We hope
these changes will make it easier to get started with packaging.

The `guix build` command has new flags, `--list-systems` and
`--list-targets`, to list supported **system types** (which may be
passed to `--system`) and **cross-compilation target triplets** (for use
with `--target`).  Under the hood, the new `(guix platform)` module lets
developers define “platforms”—a combination of CPU architecture and
operating system—in an abstract way, unifying various bits of
information previously scattered around.

In addition, packagers can now [mark as
“tunable”](https://guix.gnu.org/manual/en/html_node/Package-Transformation-Options.html#index-tunable-packages)
packages that would benefit from **CPU micro-architecture
optimizations**, enabled with `--tune`.

**Python packaging** has seen important changes.  First, the `python`
package now honors the `GUIX_PYTHONPATH` environment variable rather
than `PYTHONPATH`.  That ensures that Python won’t unwillingly pick up
packages not provided by Guix.  Second, the new
[`pyproject-build-system`](https://guix.gnu.org/manual/en/html_node/Build-Systems.html#index-pyproject_002dbuild_002dsystem)
implements [PEP 517](https://peps.python.org/pep-0517/).  It complements
the existing `python-build-system`, and both may eventually be merged
together.

What’s great with packaging is when it comes for free.  The [**`guix
import`
command**](https://guix.gnu.org/manual/en/html_node/Invoking-guix-import.html)
gained support for several upstream package repositories: `minetest`
(extensions of the Minetest game), `elm` (the Elm programming language),
`egg` (for CHICKEN Scheme), and `hexpm` (for Erlang and Elixir
packages).  Existing importers have seen various improvements.  The
[**`guix refresh`
command**](https://guix.gnu.org/manual/en/html_node/Invoking-guix-refresh.html)
to automatically update package definitions has a new `generic-git`
updater.

# Try it!

There are several ways to get started using Guix:

  1. The [installation script](https://guix.gnu.org/install.sh) lets you
     quickly install Guix [on top of another GNU/Linux
     distribution](https://guix.gnu.org/manual/en/html_node/Binary-Installation.html).

  2. The [Guix System virtual machine
     image](https://guix.gnu.org/manual/en/html_node/Running-Guix-in-a-VM.html)
     can be used with QEMU and is a simple way to discover Guix System
     without touching your system.

  3. You can [install Guix System as a standalone
     distribution](https://guix.gnu.org/manual/en/html_node/System-Installation.html).
     The
     [installer](https://guix.gnu.org/en/videos/2020/system-graphical-installer/)
     will guide you through the initial configuration steps.

To review all the installation options at your disposal, consult [the
download page](https://guix.gnu.org/en/download/) and don't hesitate
to [get in touch with us](https://guix.gnu.org/en/contact/).

Enjoy!

#### About GNU Guix

[GNU Guix](https://guix.gnu.org) is a transactional package manager and
an advanced distribution of the GNU system that [respects user
freedom](https://www.gnu.org/distros/free-system-distribution-guidelines.html).
Guix can be used on top of any system running the Hurd or the Linux
kernel, or it can be used as a standalone operating system distribution
for i686, x86_64, ARMv7, AArch64, and POWER9 machines.

In addition to standard package management features, Guix supports
transactional upgrades and roll-backs, unprivileged package management,
per-user profiles, and garbage collection.  When used as a standalone
GNU/Linux distribution, Guix offers a declarative, stateless approach to
operating system configuration management.  Guix is highly customizable
and hackable through [Guile](https://www.gnu.org/software/guile)
programming interfaces and extensions to the
[Scheme](http://schemers.org) language.
