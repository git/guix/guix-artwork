title: Meet Guix at FOSDEM
slug: meet-guix-at-fosdem-2025
date: 2025-01-21 08:30:00
author: Ludovic Courtès
tags: FOSDEM, Guix Days, Talks, Community
---

Next week will be [FOSDEM](https://fosdem.org/2025/) time for Guix!  As
in [previous years](https://guix.gnu.org/en/blog/tags/fosdem/), a
sizable delegation of Guix community members will be in Brussels.  Right
before FOSDEM, about sixty of us will gather on **January 30–31** for
the now traditional Guix Days!

![Picture showing Guix Days flag, by Luis Felipe.](/static/blog/img/guix-days-2025.webp)

In pure *unconference* style, we will self-organize and discuss and/or
hack on hot topics: drawing lessons from the [user & contributor
survey](https://guix.gnu.org/en/blog/2025/guix-user-and-contributor-survey-2024-the-results-part-1/),
improving the contributor workflow, sustaining our infrastructure,
improving governance and processes, writing the build daemon in Guile,
optimizing `guix pull`, Goblinizing the Shepherd…  there’s no shortage
of topics!

This time we’ve definitely reached the maximum capacity of our
[venue](https://www.openstreetmap.org/?mlat=50.85025&mlon=4.37326#map=19/50.85025/4.37326)
so please do not just show up if you did not
[register](https://libreplanet.org/wiki/Group:Guix/FOSDEM2025).  Next
year we’ll have to find a larger venue!

As for FOSDEM itself, here’s your agenda if you want to hear about Guix
and related projects, be it on-line or on-site.

On **Saturday, February 1st**, in the [Open Research
track](https://fosdem.org/2025/schedule/track/research/):

  - [_Guix + Software Heritage: Source Code Archiving to the Rescue of
    Reproducible
    Deployment_](https://fosdem.org/2025/schedule/event/fosdem-2025-5897-guix-software-heritage-source-code-archiving-to-the-rescue-of-reproducible-deployment/),
    at noon, where Simon Tournier will talk about the latest
    developments [connecting Guix and the Software Heritage
    archive](https://guix.gnu.org/en/blog/2024/source-code-archiving-in-guix-new-publication/).

On **Sunday, February 2nd**, *do not miss* the amazing [Declarative &
Minimalistic Computing
track](https://fosdem.org/2025/schedule/track/declarative/)!  It will
feature many Guile- and Guix-adjacent talks, in particular:

  - [_RDE: Tools for managing reproducible development
    environments_](https://fosdem.org/2025/schedule/event/fosdem-2025-5885-rde-tools-for-managing-reproducible-development-environments/),
    where Nicolas Grave will present how RDE extends Guix and what nifty
    features it brings;
  - [_The Shepherd: Minimalism in
    PID 1_](https://fosdem.org/2025/schedule/event/fosdem-2025-5720-the-shepherd-minimalism-in-pid-1/),
    where I (Ludovic Courtès) will talk about the recently-released
    [Shepherd 1.0](https://www.gnu.org/software/shepherd/news/2024/12/the-shepherd-1.0.0-released/)
    and why I think its design makes it the coolest init system to hack
    on;
  - [_Shepherd with Spritely Goblins for Secure System Layer
    Collaboration_](https://fosdem.org/2025/schedule/event/fosdem-2025-5315-shepherd-with-spritely-goblins-for-secure-system-layer-collaboration/),
    where Juliana Sims of Spritely will present on-going work to [port
    the Shepherd to
    Goblins](https://spritely.institute/news/spritely-nlnet-grants-december-2023.html)
    in support of distributed and capability-based secure computing.

But really, there’s a lot more to see in this track, starting with talks
by our Spritely friends on [web development with Guile and Hoot by David
Thompson](https://fosdem.org/2025/schedule/event/fosdem-2025-5140-minimalist-web-application-deployment-with-scheme/),
a presentation of the [Goblins distributed computing framework by
Jessica
Tallon](https://fosdem.org/2025/schedule/event/fosdem-2025-5239-goblins-the-framework-for-your-next-project-/),
and one on [Spritely’s vision by Christine Lemmer-Webber
herself](https://fosdem.org/2025/schedule/event/fosdem-2025-5123-spritely-and-a-secure-collaborative-distributed-future/)
(Spritely [will be present in other tracks
too](https://spritely.institute/news/spritely-is-going-to-guix-days-and-fosdem.html),
check it out!), as well as a [talk by Andy Wingo on what may become
Guile’s new garbage
collector](https://fosdem.org/2025/schedule/event/fosdem-2025-6066-the-whippet-embeddable-garbage-collection-library/).

Also on **Sunday, February 2nd**, jgart (Jorge Gomez) will be
presenting a survey of Immutable Linux distributions at the [Distributions
track](https://fosdem.org/2025/schedule/event/fosdem-2025-5027-building-the-future-understanding-and-contributing-to-immutable-linux-distributions/)
which will include RDE.

Good times ahead!

> Guix Days graphics are copyright © 2024 Luis Felipe López Acevedo,
> under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/),
> available from [Luis’ Guix graphics
> repository](https://codeberg.org/luis-felipe/guix-graphics).
