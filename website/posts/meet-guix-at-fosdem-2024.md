title: Guix at FOSDEM 2024
slug: meet-guix-at-fosdem-2024
date: 2024-01-19 15:00:00
author: Steve George
tags: FOSDEM, Guix Days, Talks, Community
---

It's not long to FOSDEM 2024, where Guixers will come together to learn and hack.
As usual there's some great talks and opportunities to meet other users and
contributors.

[FOSDEM](https://fosdem.org/2024/) is Europe's biggest Free Software conference.
It's aimed at developers and anyone who's interested in the Free Software
movement. While it's an in-person conference there are live video streams
and lots of ways to participate remotely.

The schedule is varied with development rooms covering many interests. Here
are some of the talks that are of particular interest to Guixers:


### Saturday, 3rd Febuary

  - ["**Making reproducible and publishable large-scale HPC experiments**"](https://fosdem.org/2024/schedule/event/fosdem-2024-2651-making-reproducible-and-publishable-large-scale-hpc-experiments/)
    by Philippe Swartvagher (10:30 CET).  Philippe will talk about the search for
    reproducible experiments in high-performance computing (HPC) and how he uses
    Guix in his methododology.


### Sunday, 4th February
The [Declarative and Minimalistic Computing track](https://fosdem.org/2024/schedule/track/declarative-and-minimalistic-computing/)
takes place Sunday morning. Important topics are:

- *Minimalism Matters*: sustainable computing through smaller, resource efficient systems
- *Declarative Programming*: reliable and reproducible systems by minimising side-effects

Guix-related talks are:

  - ["**Scheme in the Browser with Guile Hoot and WebAssembly**"](https://fosdem.org/2024/schedule/event/fosdem-2024-2339-scheme-in-the-browser-with-guile-hoot-and-webassembly/)
    by Robin Templeton (11:00 CET). A talk covering bringing Scheme to WebAssembly
    through the Guile Hoot toolchain. Addressing the current state of Guile Hoot
    with examples, and how recent Wasm proposals might improve the
    situation in the future.
  - ["**RISC-V Bootstrapping in Guix and Live-Bootstrap**"](https://fosdem.org/2024/schedule/event/fosdem-2024-1755-risc-v-bootstrapping-in-guix-and-live-bootstrap/)
    by Ekaitz Zarraga (11:20 CET).  An update on the RISC-V bootstrapping effort
    in Guix and Live-bootstrap.  Covering what's been done, what's left to do and
    some of the lessons learned.
  - ["**Self-hosting and autonomy using guix-forge**"](https://fosdem.org/2024/schedule/event/fosdem-2024-2560-self-hosting-and-autonomy-using-guix-forge/)
    by Arun Isaac (11:40 CET). This talk demonstrates the value of Guix's declarative
    configuration to simplify deploying and maintaining complex services.  Showing
    [guix-forge](https://guix-forge.systemreboot.net/), a project that
    makes it easy to self-host an efficient software forge.
  - ["**Spritely, Guile, Guix: a unified vision for user security**"](https://fosdem.org/2024/schedule/event/fosdem-2024-2331-spritely-guile-guix-a-unified-vision-for-user-security/)
    by Christine Lemmer-Webber (12:00 CET).  Spritely's goal is to create
    networked communities that puts people in control of their own identity
    and security.  This talk will present a unified vision of how Spritely,
    Guile, and Guix can work together to bring user freedom and security to
    everyone!

This year the track commemorates Joe Armstrong, who was the principal
inventor of [Erlang](https://www.erlang.org/). His focus on concurrency,
distribution and fault-tolerence are key topics in declarative and minimalistic
computing.  This [article](https://thenewstack.io/why-erlang-joe-armstrongs-legacy-of-fault-tolerant-computing/)
is a great introduction to his legacy.  Along with
["**The Mess We're In**"](https://youtu.be/lKXe3HUG2l4?si=3zbc7BEbg1o6mW5R), a
classic where he discusses why software is getting worse with time, and what can
be done about it.

On Sunday afternoon, the [Distributions devroom](https://fosdem.org/2024/schedule/track/distributions/)
has another Guix talk:

  - ["**Supporting architecture psABIs with GNU Guix**"](https://fosdem.org/2024/schedule/event/fosdem-2024-2927-supporting-architecture-psabis-with-gnu-guix/)
    by Efraim Flashner (14:30 CET).  Guix maintainer Efraim will be giving a
    talk about improving Guix's performance.  Demonstrating how to use psABI
    targets that keep older hardware compatible while providing optimized
    libraries for newer hardware.


### Guix Days (Thursday and Friday)

Guix Days will be taking place on the Thursday and Friday before FOSDEM. This is
an ["unconference-style"](https://en.wikipedia.org/wiki/Unconference) event,
where the community gets together to focus on Guix's development. All the
details are on the
[**Libreplanet Guix Wiki**](https://libreplanet.org/wiki/Group:Guix/FOSDEM2024).


### Participating

Come and join in the fun, whether you're a new Guix user or seasoned hacker!
If you're not in Brussels you can still take part:

  - See the [FOSDEM Schedule](https://fosdem.org/2024/schedule/)
  - Watch the [live streams](https://fosdem.org/2024/schedule/streaming/)
  - Chat in the unofficial [Guix Days Matrix room](https://matrix.to/#/#guix-days:matrix.org)


### About GNU Guix

[GNU Guix](https://guix.gnu.org) is a transactional package manager and
an advanced distribution of the GNU system that [respects user
freedom](https://www.gnu.org/distros/free-system-distribution-guidelines.html).
Guix can be used on top of any system running the Hurd or the Linux
kernel, or it can be used as a standalone operating system distribution
for i686, x86_64, ARMv7, AArch64, and POWER9 machines.

In addition to standard package management features, Guix supports
transactional upgrades and roll-backs, unprivileged package management,
per-user profiles, and garbage collection.  When used as a standalone
GNU/Linux distribution, Guix offers a declarative, stateless approach to
operating system configuration management.  Guix is highly customizable
and hackable through [Guile](https://www.gnu.org/software/guile)
programming interfaces and extensions to the
[Scheme](http://schemers.org) language.

