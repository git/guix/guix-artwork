#| GNU Guix manifest

This file is a GNU Guix manifest file. It can be used with GNU Guix to
create a profile or an environment to work on the project. |#

(use-modules (gnu packages))


(define PACKAGES
  (list "inkscape"
        "adwaita-icon-theme"
        "fontconfig"
        "font-juliamono"
        "font-orbitron"
        ;; Only necessary for its CMYK color profile
        "krita"))

(specifications->manifest PACKAGES)
